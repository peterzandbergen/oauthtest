package server

import "net/http"

const landingPage = `<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">

</head>
<body>
	<div class="container">
		<h1>Good Morning OAuth</h1>
		<div><a href="/admin">Protected page</a></div>
		<div><a href="/logout">Logout</a></div>
		<div><a href="/status">Status</a>
	</div>
</body>

</html>`

// handleLandingPage returns the landing page.
func (s *Server) handleLandingPage() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.log.Printf("handleLandingPage: %s", r.URL.Path)
		w.Header().Add("Content-Type", "text/html")
		w.Write([]byte(landingPage))
	}
}
