package server

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"gitlab.com/peterzandbergen/oauthtest/server/model"

	"golang.org/x/oauth2"
)

func disableCache(headers http.Header) {
	headers.Set("Cache-Control", "no-cache")
	headers.Set("Pragma", "no-cache")
}

// mwDisableCache modifies the response to disable caching.
func (s *Server) mwDisableCache(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		disableCache(w.Header())
		next(w, r)
	}
}

func (s *Server) mwSetCacheControl(next http.HandlerFunc, values ...string) http.HandlerFunc {
	const key = "Cache-Control"
	w := strings.Builder{}
	for _, v := range values {
		if w.Len() > 0 {
			w.WriteString(", ")
		}
		w.WriteString(v)
	}
	value := w.String()
	// handler
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set(key, value)
		next(w, r)
	}
}

// mwBlockPath stops processing with the given code.
func (s *Server) mwBlockPath(next http.HandlerFunc, path string, statusCode int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == path {
			w.WriteHeader(statusCode)
			return
		}
		next(w, r)
	}
}

// mwMethods only passes a request if the method is one of the methods in allowed.
// allowed is a comma separated list of allowed http methods.
func (s *Server) mwMethods(next http.HandlerFunc, allowed string) http.HandlerFunc {
	allowed = strings.ToUpper(allowed)
	methods := strings.Split(allowed, ",")
	lookup := make(map[string]struct{}, len(methods))
	for _, a := range methods {
		lookup[a] = struct{}{}
	}
	return func(w http.ResponseWriter, r *http.Request) {
		if _, ok := lookup[r.Method]; !ok {
			http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
			return
		}
		next(w, r)
	}
}

// Create a state with a unique id and a redirect to the original path.
func newState(r *http.Request) *model.OAuthState {
	return &model.OAuthState{
		ID:          fmt.Sprint(time.Now().UnixNano()),
		RedirectURL: r.URL.Path,
	}
}

func (s *Server) putState(state *model.OAuthState) (string, error) {
	if err := s.stateRepo.Put(state.ID, state); err != nil {
		return "", err
	}
	return state.ID, nil
}

// mwProtectPage redirects to login url if no token is present.
// expects token to be present in the context.
func (s *Server) mwProtectPage(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.log.Printf("mwProtectPage")
		token := getAccessToken(r.Context())
		// Redirect if we have no access token and no oauth token in the session store.
		if token == "" || s.getToken(token) == nil {
			state, err := s.putState(newState(r))
			if err != nil {
				http.Error(w, "cannot save the oauth state", http.StatusInternalServerError)
				return
			}
			authURL := s.oauthConfig.AuthCodeURL(state, oauth2.AccessTypeOffline)
			s.log.Printf("mwProtectPage: redirecting to: %s", authURL)
			http.Redirect(w, r, authURL, http.StatusMovedPermanently)
			return
		}
		next(w, r)
	}
}

func getBearerFromHeader(r *http.Request) string {
	authh := r.Header.Get("Authorization")
	if authh == "" {
		return ""
	}
	parts := strings.Split(authh, " ")
	if len(parts) != 2 {
		return ""
	}
	if parts[0] != "Bearer" {
		return ""
	}
	return parts[1]
}

func getBearerFromCookie(r *http.Request) string {
	cookies := r.Cookies()
	_ = cookies
	cookie, err := r.Cookie("Bearer")
	if err != nil {
		return ""
	}
	return cookie.Value
}

func getBearerFromQuery(r *http.Request) string {
	return r.URL.Query().Get("Bearer")
}

func (s *Server) extractToken(r *http.Request) string {
	if b := getBearerFromHeader(r); b != "" {
		s.log.Printf("mwExtractBearerToken: extracted token from header")
		return b
	}
	if b := getBearerFromCookie(r); b != "" {
		s.log.Printf("mwExtractBearerToken: extracted token from cookie")
		return b
	}
	if b := getBearerFromQuery(r); b != "" {
		s.log.Printf("mwExtractBearerToken: extracted token from query")
		return b
	}
	return ""
}

func (s *Server) mwExtractBearerToken(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.log.Printf("mwExtractBearerToken")
		if bearer := s.extractToken(r); bearer != "" {
			s.log.Printf("mwExtractBearerToken: added token to context")
			r = r.WithContext(withAuthToken(r.Context(), bearer))
		}
		next(w, r)
	}
}
