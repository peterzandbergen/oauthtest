

Skip to content
Using MyHops Mail with screen readers
label:cbs-01-peter-zandbergen 
Conversations
18.99 GB (59%) of 32 GB used
Manage
Programme Policies
Powered by Google
Last account activity: 2 hours ago
Details

LANG=en_US.UTF-8
GOVERSION=go1.15
VCAP_APPLICATION={"cf_api":"https://api.sys.nonprod02.ap.cbsp.nl","limits":{"fds":16384,"mem":25,"disk":1024},"application_name":"cfgo","application_uris":["cfgo.apps.nonprod02.ap.cbsp.nl"],"name":"cfgo","space_name":"test-space","space_id":"3af429a5-dbe7-4a68-8d2d-11e2697730ca","organization_id":"34ebafa5-81e0-40d8-8533-57746f86014d","organization_name":"cio-office","uris":["cfgo.apps.nonprod02.ap.cbsp.nl"],"process_id":"0bc2c2f7-dc45-43ad-b1fe-7e7014f56699","process_type":"web","application_id":"0bc2c2f7-dc45-43ad-b1fe-7e7014f56699","version":"8de31333-e868-4e4c-b1ec-afe1721c007d","application_version":"8de31333-e868-4e4c-b1ec-afe1721c007d"}
MEMORY_LIMIT=25m
USER=vcap
CF_INSTANCE_INTERNAL_IP=10.48.6.68
GO111MODULE=on
VCAP_APP_PORT=8080
PWD=/home/vcap
HOME=/home/vcap
CF_INSTANCE_KEY=/etc/cf-instance-credentials/instance.key
https_proxy=http://prxaltbe.cbsp.nl:8080
PORT=8080
no_proxy=.cbsp.nl
CF_INSTANCE_GUID=fb1c8e69-a94b-4df4-7f40-44a1
CF_INSTANCE_PORTS=[{"internal":8080,"external_tls_proxy":61026,"internal_tls_proxy":61001},{"internal":2222,"external_tls_proxy":61027,"internal_tls_proxy":61002}]
TERM=xterm
CF_SYSTEM_CERT_PATH=/etc/cf-system-certificates
CF_INSTANCE_IP=10.4.236.66
GODEBUG=http2debug=1
INSTANCE_INDEX=0
CF_INSTANCE_INDEX=0
SHLVL=1
INSTANCE_GUID=fb1c8e69-a94b-4df4-7f40-44a1
VCAP_SERVICES={"p-identity":[{
  "label": "p-identity",
  "provider": null,
  "plan": "cbs",
  "name": "cfgo",
  "tags": [

  ],
  "instance_name": "cfgo",
  "binding_name": null,
  "credentials": {
    "auth_domain": "https://cbs.login.sys.nonprod02.ap.cbsp.nl",
    "grant_types": [
      "authorization_code"
    ],
    "client_secret": "3a68c6f8-b516-4d96-a186-7184b55beb33",
    "client_id": "9a49bdb4-cde4-4fb5-ae20-574478f38b81"
  },
  "syslog_drain_url": null,
  "volume_mounts": [

  ]
}]}
VCAP_APP_HOST=0.0.0.0
PATH=/bin:/usr/bin
VCAP_PLATFORM_OPTIONS={"credhub-uri":"https://credhub.service.cf.internal:8844"}
CF_INSTANCE_CERT=/etc/cf-instance-credentials/instance.crt
_=/usr/bin/env
cfgo.env.txt
Displaying cfgo.env.txt.