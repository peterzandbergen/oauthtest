package server

import "net/http"

const adminPage = `<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">

</head>
<body>
	<div class="container">
	<h1>Admin Page</h1>
	<p>Admin Page</p>
	<div>    
		<div><a href="/admin">Protected page</a></div>
		<div><a href="/logout">Logout</a>
		<div><a href="/status">Status</a>
	</div>
	</div>
</body>
</html>`

func (s *Server) handleAdminPage() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		s.log.Printf("handleAdminPage")
		token := getAccessToken(r.Context())
		if token == "" {
			status := http.StatusUnauthorized
			http.Error(w, http.StatusText(status), status)
			return
		}
		w.Header().Add("Content-Type", "text/html")
		w.Write([]byte(adminPage))
	}
}
