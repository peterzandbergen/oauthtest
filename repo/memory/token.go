package memory

import (
	"sync"

	"gitlab.com/peterzandbergen/oauthtest/server/model"
)

type tokenRepo struct {
	tokens map[string]*model.Token
	mutex  sync.Mutex
}

func NewTokenRepository() model.TokenRepository {
	return &tokenRepo{
		tokens: map[string]*model.Token{},
	}
}

func (tr *tokenRepo) Put(key string, token *model.Token) error {
	tr.mutex.Lock()
	defer tr.mutex.Unlock()
	if _, err := tr.get(key); err == nil {
		return model.ErrDuplicateKey
	}
	return tr.put(key, token)
}

func (tr *tokenRepo) put(key string, token *model.Token) error {
	tr.tokens[key] = token
	return nil
}

func (tr *tokenRepo) Get(key string) (*model.Token, error) {
	tr.mutex.Lock()
	defer tr.mutex.Unlock()
	return tr.get(key)
}

func (tr *tokenRepo) get(key string) (*model.Token, error) {
	t := tr.tokens[key]
	if t == nil {
		return nil, model.ErrTokenNotFound
	}
	return t, nil
}

func (tr *tokenRepo) Delete(key string) error {
	tr.mutex.Lock()
	defer tr.mutex.Unlock()
	return tr.delete(key)
}

func (tr *tokenRepo) delete(key string) error {
	if tr.tokens[key] == nil {
		return model.ErrTokenNotFound
	}
	delete(tr.tokens, key)
	return nil
}
