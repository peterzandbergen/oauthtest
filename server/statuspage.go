package server

import (
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"

	"gitlab.com/peterzandbergen/oauthtest/server/model"
	"gitlab.com/peterzandbergen/oauthtest/server/model/util"

	"github.com/peterzandbergen/go-oidc"
)

type statusObject struct {
	Status      string
	AccessToken string
	UserID      string
	Scopes      []string
	Errors      []string
	Claims      string
}

const statusPageTmpl = `<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">

</head>
<body>
	<div class="container">
		<h1>Status</h1>
	
		<div class="row">
			<div class="col-sm">Logged</div><div class="col-sm"> {{ .Status }}</div>
		</div>
	
		<div class="row">
			<div class="col-sm">Userid</div><div class="col-sm">{{ .UserID }}</div>
		</div>
	
		<div class="row">
			<div class="col-sm">Scopes</div>
		</div>
				{{ range .Scopes }}
				<div class="row"><div class="col-sm"></div><div class="col-sm">{{ . }}</div></div>
				{{ end }}

		<div class="row">
			<div class="col-sm">================</div>
		</div>

		<div class="row">
			<div class="col-sm"><pre><code>{{ .Claims }}</code></pre></div>
		</div>

		<div class="row">
			<div class="col-sm">================</div>
		</div>
		
		<div><a href="/admin">Protected page</a></div>
		<div><a href="/logout">Logout</a></div>
		<div><a href="/status">Status</a>
	</div>

</body>
</html>`

// handleStatusPage shows the session status.
func (s *Server) handleStatusPage() http.HandlerFunc {
	// Create the template and panic if it fails.
	tmpl := template.Must(template.New("statusPage").Parse(statusPageTmpl))

	return func(w http.ResponseWriter, r *http.Request) {
		s.log.Printf("handleStatusPage")
		status := &statusObject{
			AccessToken: "empty",
			Status:      "empty",
			UserID:      "empty",
			Scopes:      []string{"empty"},
		}

		// Retrieve the access token.
		at := getAccessToken(r.Context())
		if at == "" {
			s.log.Printf("handleStatusPage: no token")
			if err := tmpl.Execute(w, &status); err != nil {
				s.log.Printf("handleStatusPage: error executing template: %s", err)
			}
			return
		}
		// Retrieve the oauth token from the session.
		ot := s.getToken(at)
		if ot == nil {
			s.log.Printf("handleStatusPage: failed to get token from sessions")
			if err := tmpl.Execute(w, &status); err != nil {
				s.log.Printf("handleStatusPage: error executing template: %s", err)
			}
			return
		}
		// Parse the id token.
		idToken, err := s.parseIDToken(ot.IDToken)
		if err != nil {
			status.Errors = append(status.Errors, fmt.Sprintf("error parsing id token: %s", err))
			if err := tmpl.Execute(w, &status); err != nil {
				s.log.Printf("handleStatusPage: error executing template: %s", err)
			}
			return
		}
		// Get the key.
		// key := s.oidcClient.Keys.Key(jt.Headers[0].KeyID)[0]
		idClaims := map[string]interface{}{}
		if err := idToken.Claims(&idClaims); err != nil {
			if err := tmpl.Execute(w, &status); err != nil {
				s.log.Printf("handleStatusPage: error executing template: %s", err)
			}
			return
		}

		b, err := json.MarshalIndent(idClaims, "", "    ")
		if err != nil {
			if err := tmpl.Execute(w, &status); err != nil {
				s.log.Printf("handleStatusPage: error executing template: %s", err)
			}
			return
		}
		status.Claims = string(b)

		status.AccessToken = at
		status.Status = "IN"
		if s, ok := idClaims["user_name"].(string); ok {
			status.UserID = s
		}
		status.Scopes = toStringArray(idClaims["scope"])
		if err := tmpl.Execute(w, &status); err != nil {
			s.log.Printf("handleStatusPage: error executing template: %s", err)
		}
		s.logUserInfo(ot)
	}
}

func toStringArray(arr interface{}) []string {
	iarr, ok := arr.([]interface{})
	if !ok {
		return nil
	}
	res := make([]string, len(iarr))
	for i, v := range iarr {
		res[i] = v.(string)
	}
	return res
}

func (s *Server) parseIDToken(rawToken string) (*oidc.IDToken, error) {
	idToken, err := s.idTokenVerifier().Verify(context.Background(), rawToken)
	if err != nil {
		return nil, fmt.Errorf("parseIDToken: verify error %w", err)
	}
	return idToken, nil
}

func (s *Server) getUserInfo(token *model.Token) (*oidc.UserInfo, error) {
	ctx := context.Background()
	tokenSource := s.oauthConfig.TokenSource(ctx, util.ToOAuthToken(token))
	userInfo, err := s.provider.UserInfo(ctx, tokenSource)
	if err != nil {
		return nil, err
	}
	return userInfo, nil
}

func (s *Server) logUserInfo(token *model.Token) {
	// Get the user info
	userInfo, err := s.getUserInfo(token)
	if err != nil {
		s.log.Printf("logUserInfo: error getting user info from remote: %s", err)
		return
	}

	// Unmarshal the claims
	userInfoClaims := map[string]interface{}{}
	if err := userInfo.Claims(&userInfoClaims); err != nil {
		s.log.Printf("logUserInfo: error unmarshalling user info claims: %s", err)
	}
	// Format the json
	b, err := json.MarshalIndent(userInfoClaims, "", "    ")
	if err != nil {
		s.log.Printf("logUserInfo: error executing template: %s", err)
		return
	}
	// Log the json
	s.log.Printf("logUserInfo: the user info from remote server: \n%s\n", string(b))
}
