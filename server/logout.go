package server

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"golang.org/x/oauth2"
)

func (s *Server) handleLogout() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.log.Printf("handleLogout")
		redirectURL := func(res string) string {
			// get the access token.
			at := getAccessToken(r.Context())
			if at == "" {
				s.log.Printf("handleLogout: no access key in context")
				return res
			}
			// get the oauth token
			ot := s.getToken(at)
			if ot == nil {
				s.log.Printf("handleLogout: no access token in server.sessions")
				return res
			}
			// end session
			res, err := s.logoutURL(ot.IDToken, s.baseURL + "/", "")
			if err != nil {
				s.log.Printf("handleLogout: error getting logout url: %s", err)
				res = "/"
			}
			s.deleteToken(at)
			return res
		}("/")

		http.SetCookie(w, &http.Cookie{
			Name:    "Bearer",
			Value:   "",
			Expires: time.Now().Add(-time.Hour),
		})
		w.Header().Add("Content-Type", "text/html")
		s.log.Printf("handleLogout: logging out, redirecting to %s", redirectURL)
		http.Redirect(w, r, redirectURL, http.StatusMovedPermanently)
	}
}

func (s *Server) logoutURL(idToken, redirectURL, state string) (string, error) {
	if s.onCF {
		return s.cfLogoutURL(redirectURL)
	}
	return s.discoveryLogoutURL(idToken, redirectURL, state)
}

func (s *Server) discoveryLogoutURL(idToken, redirectURL, state string) (string, error) {
	if len(s.providerClaims.EndSessionEndpoint) == 0 {
		return "", errors.New("no end session endpoint available")
	}
	if len(idToken) == 0 {
		return "", errors.New("no id token available")
	}
	values := url.Values{}
	values.Set("id_token_hint", idToken)
	if redirectURL != "" {
		values.Set("post_logout_redirect_uri", redirectURL)
	}
	if state != "" {
		values.Set("state", state)
	}
	u, err := url.Parse(s.providerClaims.EndSessionEndpoint)
	if err != nil {
		return "", fmt.Errorf("logoutURL: error parsing EndSessionEndpoint: %w", err)
	}
	u.RawQuery = values.Encode()
	return u.String(), nil
}

func (s *Server) idTokenRaw(at *oauth2.Token) string {
	rawToken, ok := at.Extra("id_token").(string)
	if !ok {
		return ""
	}
	return rawToken
}

func (s *Server) cfLogoutURL(redirectURL string) (string, error) {
	s.log.Printf("cfLogoutURL: started")
	values := url.Values{}
	values.Set("client_id", s.oauthConfig.ClientID)
	if redirectURL != "" {
		values.Set("redirect", redirectURL)
	}
	authURL, err := url.Parse(s.providerClaims.AuthorizationEndpoint)
	if err != nil {
		return "", fmt.Errorf("cfLogoutURL: cannot parse authorization endpoint: %w", err)
	}
	u := &url.URL{
		Scheme: authURL.Scheme,
		Host:   authURL.Host,
		Path:   "/logout.do",
	}
	u.RawQuery = values.Encode()
	return u.String(), nil
}

