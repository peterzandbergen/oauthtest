package redis

import (
	"crypto/sha1"
	"encoding/hex"
	"io"
)

func hashKey(key string) string {
	h := sha1.New()
	io.WriteString(h, key)
	return hex.EncodeToString(h.Sum(nil))
}
