package main

import (
	"bufio"
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"strings"

	redigo "github.com/gomodule/redigo/redis"
	"github.com/peterzandbergen/go-oidc"
	"golang.org/x/oauth2"

	"gitlab.com/peterzandbergen/oauthtest/cf"
	"gitlab.com/peterzandbergen/oauthtest/repo/memory"
	"gitlab.com/peterzandbergen/oauthtest/repo/redis"
	server "gitlab.com/peterzandbergen/oauthtest/server"
	"gitlab.com/peterzandbergen/oauthtest/server/model"
)

type config struct {
	discoveryURI  string
	hostname      string
	port          int
	clientID      string
	clientSecret  string
	scheme        string
	serverBaseURL string
	scopes        string
	onCf          bool
	redisServer   string
	skipVerify    bool
}

func (cfg *config) ListenAddress() string {
	return fmt.Sprintf("0.0.0.0:%d", cfg.port)
}

func mask(val string) string {
	return strings.Repeat("*", len(val))
}

func (cfg *config) String() string {
	w := strings.Builder{}
	fmt.Fprintf(&w, "main/config:")
	fmt.Fprintf(&w, "\n    discoveryURI: %s", cfg.discoveryURI)
	fmt.Fprintf(&w, "\n    hostname: %s", cfg.hostname)
	fmt.Fprintf(&w, "\n    port: %d", cfg.port)
	fmt.Fprintf(&w, "\n    clientID: %s", cfg.clientID)
	fmt.Fprintf(&w, "\n    clientSecret: %s", mask(cfg.clientSecret))
	fmt.Fprintf(&w, "\n    scheme: %s", cfg.scheme)
	fmt.Fprintf(&w, "\n    serverBaseURL: %s", cfg.serverBaseURL)
	fmt.Fprintf(&w, "\n    scopes: %s", cfg.scopes)
	fmt.Fprintf(&w, "\n    skipVerify: %v", cfg.skipVerify)
	return w.String()
}

func configFromEnv(env []string) *config {
	m := map[string]string{}
	for _, e := range env {
		i := strings.Index(e, "=")
		if i > 0 && len(e) > i {
			m[e[:i]] = e[i+1:]
			log.Printf("added to config from env: %s=%s", e[:i], e[i+1:])
		}

	}
	return configFromMap(m)
}

func configFromMap(env map[string]string) *config {
	cfg := &config{}
	for k, v := range env {
		switch k {
		case "CFGO_DISCOVERY_URI":
			cfg.discoveryURI = v
		case "CFGO_SERVER_NAME":
			cfg.hostname = v
		case "CFGO_CLIENT_ID":
			cfg.clientID = v
		case "CFGO_CLIENT_SECRET":
			cfg.clientSecret = v
		case "CFGO_CALLBACK_SCHEME":
			cfg.scheme = v
		case "CFGO_OAUTH_SCOPES":
			log.Printf("scopes from env: %s", v)
			cfg.scopes = v
		case "CFGO_REDIS_ADDRESS":
			cfg.redisServer = v
		case "CFGO_SKIP_VERIFY":
			cfg.skipVerify = v == "1" || v == "y" || v == "Y"
		case "PORT":
			cfg.port = 8080
			if p, err := strconv.Atoi(v); err == nil {
				cfg.port = p
			}
		}
	}
	// base url
	baseURL := url.URL{}
	baseURL.Scheme = cfg.scheme
	baseURL.Host = fmt.Sprintf("%s:%d", cfg.hostname, cfg.port)
	cfg.serverBaseURL = baseURL.String()
	return cfg
}

func newMainLogger() *log.Logger {
	return log.New(os.Stderr, "", log.LstdFlags|log.Lmicroseconds)
}

func configFromCfEnv() (*config, error) {
	log.Println("configFromCfEnv: start")
	cfg := &config{
		onCf: true,
	}
	env := cf.Env()
	creds := env.PIdentityCredentials()
	if len(creds) < 1 {
		return nil, errors.New("configFromEnv: no creds found in config")
	}
	cfg.clientID = creds[0].ClienID
	cfg.clientSecret = creds[0].ClientSecret
	cfg.port = env.Port()
	cfg.hostname = env.VcapApplication.Uris[0]
	cfg.scopes = env.Env["CFGO_OAUTH_SCOPES"]
	// discovery url
	cfg.discoveryURI = creds[0].AuthDomain
	// base url
	baseURL := url.URL{}
	baseURL.Scheme = "https"
	baseURL.Host = cfg.hostname
	cfg.serverBaseURL = baseURL.String()
	return cfg, nil
}

func skipVerifyHTTPClient(skip bool) *http.Client {
	tlsCfg := &tls.Config{
		InsecureSkipVerify: skip,
	}
	return &http.Client{
		Transport: server.DefaultTransportWithTLSConfig(tlsCfg),
	}
}

func newProvider(ctx context.Context, issuer string, skip bool) (*oidc.Provider, error) {
	// skip tls verification
	ctx = context.WithValue(ctx, oauth2.HTTPClient, skipVerifyHTTPClient(skip))

	// try strict issuer check
	provider, err := oidc.NewProvider(ctx, issuer,
		oidc.WithIssuerCheck(true))
	if err == nil {
		return provider, nil
	}
	log.Printf("newProvider: issuer check failed: %s", err)
	// try with relaxed issuer check
	provider, err = oidc.NewProvider(ctx, issuer,
		oidc.WithIssuerCheck(false))
	if err != nil {
		return nil, err
	}
	return provider, err
}

func logProvider(prov *oidc.Provider) {
	object := map[string]interface{}{}
	if err := prov.Claims(&object); err != nil {
		return
	}
	b, err := json.MarshalIndent(object, "", "    ")
	if err != nil {
		return
	}
	log.Printf("discovery object:\n%s\n", string(b))
}

func configFromFile(name string) (*config, error) {
	r, err := os.Open(name)
	if err != nil {
		return nil, err
	}
	defer r.Close()
	var res []string
	s := bufio.NewScanner(r)
	for s.Scan() {
		res = append(res, s.Text())
	}
	return configFromEnv(res), nil
}

func getConfig() (*config, error) {
	// First test for a command line parameter
	if len(os.Args) == 2 {
		return configFromFile(os.Args[1])
	}
	onCf := cf.OnEnvironment()
	switch {
	case onCf:
		return configFromCfEnv()
	default:
		return configFromEnv(os.Environ()), nil
	}
}

func createRepos(cfg *config) (model.TokenRepository, model.OAuthStateRepository, error) {
	if cfg.redisServer == "" {
		return memory.NewTokenRepository(), memory.NewOAuthStateRepository(), nil
	}
	conn, err := redigo.Dial("tcp", cfg.redisServer)
	if err != nil {
		return nil, nil, err
	}
	return redis.NewTokenRepository(conn), redis.NewOAuthStateRepository(conn), nil
}

func run(cfg *config) {
	logger := newMainLogger()
	log.Println(cfg.String())
	// Retrieve the server metadata.
	// Disable issuer check when on CF.
	provider, err := newProvider(context.Background(), cfg.discoveryURI, cfg.skipVerify)
	if err != nil {
		log.Fatalf("error retrieving provider from %s: %s", cfg.discoveryURI, err)
	}
	logProvider(provider)
	mustString := func(s string, err error) string {
		if err != nil {
			panic(fmt.Sprintf("mustString failed: %s", err.Error()))
		}
		return s
	}
	oauth2Config := oauth2.Config{
		ClientID:     cfg.clientID,
		ClientSecret: cfg.clientSecret,
		RedirectURL:  mustString(url.JoinPath(cfg.serverBaseURL + server.CallbackPath)),

		Endpoint: provider.Endpoint(),
		Scopes:   strings.Split(cfg.scopes, " "),
	}
	log.Printf("redirect url: %s", oauth2Config.RedirectURL)
	claims := map[string]interface{}{}
	provider.Claims(&claims)
	_ = claims
	tokenRepo, stateRepo, err := createRepos(cfg)
	if err != nil {
		log.Fatalf("unable to contact redis on %s: %s", cfg.redisServer, err)
	}
	server := server.New(
		server.WithOidcConfig(&oidc.Config{
			ClientID:        cfg.clientID,
			SkipIssuerCheck: true}),
		server.WithOidcProvider(provider),
		server.WithLogger(logger),
		server.WithBaseURL(cfg.serverBaseURL),
		server.WithOAuthConfig(&oauth2Config),
		server.OnCf(cfg.onCf),
		server.WithTokenRepo(tokenRepo),
		server.WithOAuthStateRepo(stateRepo),
		server.WithSkipVerify(cfg.skipVerify),
	)
	hs := &http.Server{
		Addr:     cfg.ListenAddress(),
		Handler:  server,
		ErrorLog: log.New(os.Stderr, "", log.LstdFlags),
	}
	go hs.ListenAndServe()

	logger.Printf("server started, listening on %s", cfg.ListenAddress())
	waitc := make(chan os.Signal, 1)
	signal.Notify(waitc, os.Interrupt, os.Kill)
	caught := <-waitc
	hs.Close()
	logger.Printf("server stopped: %s", caught.String())
}

func main() {
	cfg, err := getConfig()
	if err != nil {
		log.Fatalf("error getting config: %s", err)
	}
	run(cfg)
}
