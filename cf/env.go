package cf

import (
	"encoding/json"
	"errors"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"
)

type CfEnv struct {
	Env             map[string]string
	VcapApplication *VcapApplication
	VcapServices    VcapServices
	CfInstancePorts []*InstancePort
}

type InstancePort struct {
	External int `json:"external"`
	Internal int `json:"internal"`
}

type VcapApplication struct {
	CfAPI              string    `json:"cf_api"`
	Limits             Limits    `json:"limits"`
	ApplicationName    string    `json:"application_name"`
	ApplicationUris    []string  `json:"application_uris"`
	Name               string    `json:"name"`
	SpaceName          string    `json:"space_name"`
	SpaceID            string    `json:"space_id"`
	OrganizationID     string    `json:"organization_id"`
	OrganizationName   string    `json:"organization_name"`
	Uris               []string  `json:"uris"`
	ProcessID          string    `json:"process_id"`
	ProcessType        string    `json:"process_type"`
	ApplicationID      string    `json:"application_id"`
	Version            string    `json:"version"`
	ApplicationVersion string    `json:"application_version"`
	Start              string    `json:"start"`
	StartedAt          string    `json:"started_at"`
	StartedAtTimeStamp time.Time `json:"started_at_timestamp"`
	StateTimeStamp     time.Time `json:"state_timestamp"`
}
type Limits struct {
	Fds  int `json:"fds"`
	Mem  int `json:"mem"`
	Disk int `json:"disk"`
}

type PIdentityCredentials struct {
	AuthDomain   string   `json:"auth_domain,omitempty"`
	GrantTypes   []string `json:"grant_types,omitempty"`
	ClienID      string   `json:"client_id,omitempty"`
	ClientSecret string   `json:"client_secret,omitempty"`
}

type Service struct {
	Name            string          `json:"name"`
	BindingName     string          `json:"binding_name"`
	InstanceName    string          `json:"instance_name"`
	Label           string          `json:"label"`
	Tags            []string        `json:"tags"`
	Plan            string          `json:"plan"`
	CredentialsJSON json.RawMessage `json:"credentials"`
	Credentials     interface{}     `json:"-"`
}

type VcapServices map[string][]*Service

func mapEnv(env []string) map[string]string {
	m := map[string]string{}
	for _, e := range env {
		i := strings.Index(e, "=")
		if i > 0 && len(e) > i {
			m[e[:i]] = e[i+1:]
		}
	}
	return m
}

func Env() *CfEnv {
	return env(os.Environ())
}

func env(environ []string) *CfEnv {
	env := &CfEnv{}
	env.Env = mapEnv(environ)
	// Parse the json structs.
	env.parseVcapApplication()
	env.parseVcapServices()
	return env
}

func (e *CfEnv) parseVcapApplication() {
	val := e.Env["VCAP_APPLICATION"]
	if len(val) == 0 {
		return
	}
	app := &VcapApplication{}
	if err := json.Unmarshal([]byte(val), app); err != nil {
		return
	}
	e.VcapApplication = app
}

func (e *CfEnv) parseVcapServices() {
	val := e.Env["VCAP_SERVICES"]
	if len(val) == 0 {
		return
	}
	svc := VcapServices{}
	if err := json.Unmarshal([]byte(val), &svc); err != nil {
		return
	}
	e.VcapServices = svc
}

func (e *CfEnv) PIdentityCredentials() []*PIdentityCredentials {
	var res []*PIdentityCredentials
	for k, svcs := range e.VcapServices {
		if k != "p-identity" {
			continue
		}
		for _, svc := range svcs {
			// unmarshal creds
			creds := &PIdentityCredentials{}
			if err := json.Unmarshal(svc.CredentialsJSON, creds); err != nil {
				continue
			}
			res = append(res, creds)
		}
	}
	return res
}

func parsePIdentityServices(val string) ([]*PIdentityCredentials, error) {
	it := reflect.TypeOf(PIdentityCredentials{})
	builders := map[string]reflect.Type{
		"p-identity": it,
	}

	svc, err := parseVcapServices(val, builders)
	if err != nil {
		return nil, err
	}
	var res []*PIdentityCredentials
	idps := svc["p-identity"]
	if idps == nil {
		return nil, errors.New("no services found")
	}
	for _, idp := range idps {
		creds, ok := idp.Credentials.(*PIdentityCredentials)
		if !ok {
			continue
		}
		res = append(res, creds)
	}
	if len(res) == 0 {
		return nil, errors.New("no services found")
	}
	return res, nil
}

func parseVcapServices(val string, credTypes map[string]reflect.Type) (VcapServices, error) {
	services := VcapServices{}
	err := json.Unmarshal([]byte(val), &services)
	if err != nil {
		return nil, err
	}
	// Unmarshal the credentials.
	for k, sa := range services {
		// unmarshal to service and replace if successful.
		for i, s := range sa {
			// create new instance
			t := credTypes[s.Label]
			if t == nil {
				continue
			}
			obj := reflect.New(t).Interface()
			err := json.Unmarshal(s.CredentialsJSON, obj)
			if err != nil {
				continue
			}
			services[k][i].Credentials = obj
		}
	}
	return services, nil
}

func OnEnvironment() bool {
	val, present := os.LookupEnv("CF_INSTANCE_GUID")
	return present && onEnvironment(val)
}

func onEnvironment(val string) bool {
	return len(val) > 0
}

func (e *CfEnv) Port() int {
	res := 8080
	ps := e.Env["PORT"]
	if p, err := strconv.Atoi(ps); err == nil {
		res = p
	}
	return res
}
