package util

import (
	"gitlab.com/peterzandbergen/oauthtest/server/model"

	"golang.org/x/oauth2"
)

func FromOAuthToken(token *oauth2.Token) *model.Token {
	idtoken, _ := token.Extra("id_token").(string)
	return &model.Token{
		AccessToken:  token.AccessToken,
		Expiry:       token.Expiry,
		RefreshToken: token.RefreshToken,
		TokenType:    token.TokenType,
		IDToken:      idtoken,
	}
}

func ToOAuthToken(token *model.Token) *oauth2.Token {
	return &oauth2.Token{
		AccessToken:  token.AccessToken,
		Expiry:       token.Expiry,
		RefreshToken: token.RefreshToken,
		TokenType:    token.TokenType,
	}
}
