package model

import (
	"errors"
	"time"
)

type NonceRepository interface {
	Put(nonce string) error
	Get(nonce string, delete ...bool) error
	Delete(nonce string) error
}

// OAuthState is used for storing the state of the oauth redirect.
type OAuthState struct {
	ID          string
	RedirectURL string
}

// OAuthStateRepository stores states. A state is an opaque byte array
// that typically holds a json string.
type OAuthStateRepository interface {
	Put(key string, state *OAuthState) error
	Get(key string, delete ...bool) (*OAuthState, error)
	Delete(key string) error
}

var (
	ErrTokenNotFound = errors.New("token not found")
	ErrDuplicateKey  = errors.New("duplicate key")
)

type Token struct {
	AccessToken  string
	TokenType    string
	RefreshToken string
	Expiry       time.Time
	IDToken      string
}

type TokenRepository interface {
	Put(key string, token *Token) error
	Get(key string) (*Token, error)
	Delete(key string) error
}
