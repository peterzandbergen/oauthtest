package server

import (
	"crypto/tls"
	"log"
	"net/http"
	"strings"

	"gitlab.com/peterzandbergen/oauthtest/repo/memory"
	"gitlab.com/peterzandbergen/oauthtest/server/model"

	"github.com/peterzandbergen/go-oidc"
	"golang.org/x/oauth2"
)

type contextKey int

const (
	authTokenKey = contextKey(iota + 1)
)

type ProviderClaims struct {
	Issuer                                    string   `json:"issuer"`
	AuthorizationEndpoint                     string   `json:"authorization_endpoint"`
	TokenEndpoint                             string   `json:"token_endpoint"`
	RegistrationEndpoint                      string   `json:"registration_endpoint"`
	JwksURI                                   string   `json:"jwks_uri"`
	ResponseTypesSupported                    []string `json:"response_types_supported"`
	ResponseModesSupported                    []string `json:"response_modes_supported"`
	GrantTypesSupported                       []string `json:"grant_types_supported"`
	SubjectTypesSupported                     []string `json:"subject_types_supported"`
	ScopesSupported                           []string `json:"scopes_supported"`
	TokenEndpointAuthMethodsSupported         []string `json:"token_endpoint_auth_methods_supported"`
	ClaimsSupported                           []string `json:"claims_supported"`
	CodeChallengeMethodsSupported             []string `json:"code_challenge_methods_supported"`
	IntrospectionEndpoint                     string   `json:"introspection_endpoint"`
	IntrospectionEndpointAuthMethodsSupported []string `json:"introspection_endpoint_auth_methods_supported"`
	RevocationEndpoint                        string   `json:"revocation_endpoint"`
	RevocationEndpointAuthMethodsSupported    []string `json:"revocation_endpoint_auth_methods_supported"`
	EndSessionEndpoint                        string   `json:"end_session_endpoint"`
	RequestParameterSupported                 bool     `json:"request_parameter_supported"`
	RequestObjectSigningAlgValuesSupported    []string `json:"request_object_signing_alg_values_supported"`
}

type Server struct {
	router  http.ServeMux
	baseURL string
	// sigKeys map[string]interface{}
	log  *log.Logger
	onCF bool

	tokenRepo model.TokenRepository
	stateRepo model.OAuthStateRepository

	provider       *oidc.Provider
	providerClaims *ProviderClaims
	oidcConfig     *oidc.Config
	oauthConfig    *oauth2.Config
	verifier       *oidc.IDTokenVerifier

	exchangeHTTPClient *http.Client
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

func (s *Server) BaseURL() string {
	return s.baseURL
}

type option func(*Server)

func (s *Server) idTokenVerifier() *oidc.IDTokenVerifier {
	if s.verifier == nil {
		s.verifier = s.provider.Verifier(s.oidcConfig)
	}
	return s.verifier
}

func WithLogger(logger *log.Logger) func(*Server) {
	return func(s *Server) {
		s.log = logger
	}
}

func WithBaseURL(baseURL string) func(*Server) {
	return func(s *Server) {
		s.baseURL = baseURL
	}
}

// func WithScopes(scopes string) func(*Server) {
// 	return func(s *Server) {
// 		s.scopes = scopes
// 	}
// }

func WithOidcProvider(provider *oidc.Provider) func(*Server) {
	return func(s *Server) {
		s.provider = provider
		pc := &ProviderClaims{}
		provider.Claims(pc)
		s.providerClaims = pc
	}
}

func WithOAuthConfig(config *oauth2.Config) func(*Server) {
	return func(s *Server) {
		s.oauthConfig = config
	}
}

// DefaultTransportWithTLSConfig returns a default transport with
// TLSClientConfig set to tlscfg.
func DefaultTransportWithTLSConfig(tlscfg *tls.Config) *http.Transport {
	if tlscfg == nil {
		return nil
	}
	t := CloneDefaultTransport()
	t.TLSClientConfig = tlscfg
	return t
}

// CloneDefaultTransport a clone of http.DefaultTransport.
func CloneDefaultTransport() *http.Transport {
	transport := http.DefaultTransport.(*http.Transport)
	return transport.Clone()
}

func WithSkipVerify(skip bool) func(*Server) {
	return func(s *Server) {
		tlsCfg := &tls.Config{
			InsecureSkipVerify: skip,
		}
		s.exchangeHTTPClient = &http.Client{
			Transport: DefaultTransportWithTLSConfig(tlsCfg),
		}
	}
}

func WithOidcConfig(config *oidc.Config) func(*Server) {
	return func(s *Server) {
		s.oidcConfig = config
	}
}

func WithTokenRepo(repo model.TokenRepository) func(*Server) {
	return func(s *Server) {
		s.tokenRepo = repo
	}
}

func WithOAuthStateRepo(repo model.OAuthStateRepository) func(*Server) {
	return func(s *Server) {
		s.stateRepo = repo
	}
}

func OnCf(yes bool) func(*Server) {
	return func(s *Server) {
		s.onCF = yes
	}
}

func New(options ...option) *Server {
	// create a new server
	s := &Server{}
	// apply the options
	for _, opt := range options {
		opt(s)
	}
	// check the server for missing information
	if s.oidcConfig == nil {
		s.oidcConfig = &oidc.Config{
			ClientID: s.oauthConfig.ClientID,
		}
	}
	// Create the token repo if none set.
	if s.tokenRepo == nil {
		s.tokenRepo = memory.NewTokenRepository()
	}
	if s.stateRepo == nil {
		s.stateRepo = memory.NewOAuthStateRepository()
	}
	// create the routes
	s.routes()
	return s
}

func (s *Server) allScopes() string {
	var w strings.Builder
	for _, s := range s.oauthConfig.Scopes {
		if w.Len() > 0 {
			w.WriteString(" ")
		}
		w.WriteString(s)
	}
	return w.String()
}
