package nonce

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/binary"
	"time"
)

const (
	saltSize    = 2
	counterSize = 2
)

func New() (string, error) {
	h := sha256.New()
	b := make([]byte, saltSize)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	h.Write(b)
	// append the time in nano seconds
	tn := time.Now().UnixNano()
	b = make([]byte, binary.MaxVarintLen64)
	l := binary.PutVarint(b, tn)
	h.Write(b[:l])
	hb := h.Sum(nil)
	// res := hex.EncodeToString(hb)
	res := base64.RawURLEncoding.EncodeToString(hb)
	return res, nil
}
