
# OpenID Connect Middelware

This middleware handles the athentication of a user using oauth.

It acts as a middleware and injects the authentication information
in the context.

## Okta

We use Okta as test platform and the oauth playground to see how oauth works.

[Okta Portal](https://dev-9673843-admin.okta.com/dev/console)

[OAuth Playground](https://www.oauth.com/playground)

## Useful tools

[JSON-to Go](https://mholt.github.io/json-to-go/): creates Go structs for marshalling and unmarshalling JSON files.


## Tar Tricks

Compress oauthtest directory to tgz and base64 encode it. Do not wrap the lines to avoid LF CRLF problems.

Next command creates an archive without the hidden files and directories. If you use ```oauthtest``` as tar 
input name to include the hidden files. 

**No hidden files**
```
tar -cz --to-stdout oauthtest/* | base64 --wrap=0 > oauthtest.tgz.b64
```

**With hidden files**
```
tar -cz --to-stdout oauthtest | base64 --wrap=0 > oauthtest.tgz.b64
```



Expand the archive to an existing directory. This creates the oauthtest directory.

```
base64 -d oauthtest.tgz.b64 | tar -C /tmp/create-dir-in-here -xzf -
```

## SAML 2.0 en Keycloak test

Follow these steps to setup a test environment with samltest.id:

- start a Keycloak server in Docker
- create a new realm in Keycloak
- create a client application in the new realm
- add a new identity provider for SAML 2.0
- upload the SAML configuration to samltest.id
- configure OAuthTest to use the local Keycloak server

### Start Keycloak in Docker

```
docker run --name keycloak-server -p 8080:8080 -e KEYCLOAK_USER=peza -e KEYCLOAK_PASSWORD=secret jboss/keycloak
```

You can change the host port for Keycloak with the -p parameter, e.g. ```-p 18080:8080```.

Test if you can login to http://localhost:8080.

### Create a new realm in Keycloak

Log in to Keycloak and create a new realm. Follow the manual for further details.

Name the realm e.g. SAML Test

### Create a client application

The client application will be oauthtest and it will listen on http://localhost:18080

### Create a redis service

```
docker run -p 6379:6379 --name redis-rest redis:alpine
```