package main

import (
	"log"
	"net/http"
	"os"
	"path/filepath"
)

func main() {
	dir  := "."
	if l := len(os.Args); l == 2 {
		dir = os.Args[1]
	}
	// create file server handler
	fs := http.FileServer(http.Dir(filepath.Clean(dir)))

	// start HTTP server with `fs` as the default handler
	const address = ":8080"
	log.Printf("serving files from: %s", dir)
	log.Printf("listening on %s", address)

	log.Fatal(http.ListenAndServe(address, fs))
}
