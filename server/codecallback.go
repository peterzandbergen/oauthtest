package server

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/peterzandbergen/oauthtest/server/model"
	"gitlab.com/peterzandbergen/oauthtest/server/model/util"

	"golang.org/x/oauth2"
)

const CallbackPath = "/authorization-code/callback"

// handleCodeCallback takes the code from the query and exchanges it for a token.
// It then returns the access token as cookie with name Bearer (case sensive).
func (s *Server) handleCodeCallback() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		s.log.Printf("handleCodeCallback: %s", r.URL.String())
		code := r.URL.Query().Get("code")
		if len(code) == 0 {
			s.log.Printf("handleCodeCallback: received empty code")
			http.Redirect(w, r, "/", http.StatusMovedPermanently)
			return
		}
		s.log.Printf("handleCodeCallback: exchanging code for token, code=%s", code)
		// Get context with custom http client.
		ctx := context.WithValue(context.Background(), oauth2.HTTPClient, s.exchangeHTTPClient)
		ac, err := s.oauthConfig.Exchange(ctx, code)
		if err != nil {
			s.log.Printf("handleCodeCallback: error retrieving oauth token: %s", err)
			http.Redirect(w, r, "/", http.StatusMovedPermanently)
			return
		}

		s.log.Printf("handleCodeCallback: oauth token: %s", ac.AccessToken)
		// Add bearer token as cookie to response.
		http.SetCookie(w, &http.Cookie{
			Name:     "Bearer",
			Value:    ac.AccessToken,
			HttpOnly: true,
			SameSite: http.SameSiteLaxMode,
			Path:     "/",
		})
		// save oauth access token in sessions.
		s.putToken(ac)
		// get redirect url from state in repo.
		redirectURL := "/"
		state, err := s.stateRepo.Get(r.URL.Query().Get("state"), true)
		if err != nil {
			s.log.Printf("error retrieving state from repo: %s", err)
		} else {
			redirectURL = state.RedirectURL
		}
		http.Redirect(w, r, redirectURL, http.StatusMovedPermanently)
		s.logJWTToken("handleCodeCallback: accessToken", ac.AccessToken)
		return
	}
}

func unmarshallB64JSON(s string, dest interface{}) error {
	b, err := base64.RawURLEncoding.DecodeString(s)
	if err != nil {
		return fmt.Errorf("unmarshallB64JSON: failed to unmarahall [%s]: %w", s, err)
	}
	return json.Unmarshal(b, dest)
}

func parseJWTToJSON(signature string) ([]string, error) {
	// Split parts on .
	parts := strings.Split(signature, ".")
	if l := len(parts); l != 3 {
		return nil, fmt.Errorf("parseJWTToJSON: expected 3 parts, got %d", l)
	}
	// base64 decode part 0 and 1
	res := make([]string, 2)
	// Unmarshal nand MarshalIndent the parts and return
	for i, s := range parts[:2] {
		obj := map[string]interface{}{}
		if err := unmarshallB64JSON(s, &obj); err != nil {
			return nil, fmt.Errorf("parseJWTToJSON: error unmarshalling part %d: %w", i, err)
		}
		b, err := json.MarshalIndent(obj, "", "    ")
		if err != nil {
			return nil, fmt.Errorf("parseJWTToJSON: error marshalling part %d: %w", i, err)
		}
		res[i] = string(b)
	}
	return res, nil
}

func (s *Server) logJWTToken(tokenName, signature string) {
	j, err := parseJWTToJSON(signature)
	if err != nil {
		s.log.Printf("logJWTToken: error parsing token: %s", err)
		return
	}
	s.log.Printf("%s\n%s\n.\n%s\n", tokenName, j[0], j[1])
}

// putToken adds or updates the token in sessions.
func (s *Server) putToken(oauthToken *oauth2.Token) {
	if err := s.tokenRepo.Put(oauthToken.AccessToken, util.FromOAuthToken(oauthToken)); err != nil {
		s.log.Printf("putToken: error saving token: %s", err)
	}
}

// getToken retrieves the token from the sessions.
// It returns nil if no token with the given access token exists.
func (s *Server) getToken(accessToken string) *model.Token {
	res, err := s.tokenRepo.Get(accessToken)
	if err != nil {
		s.log.Printf("getToken: error retrieving token: %s", err)
		return nil
	}
	return res
}

// deleteToken deletes the token if it exists.
func (s *Server) deleteToken(accessToken string) {
	if err := s.tokenRepo.Delete(accessToken); err != nil {
		s.log.Printf("deleteToken: error deleting token: %s", err)
	}
}

// withAuthToken returns a context with the token value.
func withAuthToken(ctx context.Context, v string) context.Context {
	return context.WithValue(ctx, authTokenKey, v)
}

// getAccessToken returns the access token from the context.
// It returns an empty string if it does not exist.
func getAccessToken(ctx context.Context) string {
	v, ok := ctx.Value(authTokenKey).(string)
	if !ok {
		v = ""
	}
	return v
}
