package memory

import (
	"gitlab.com/peterzandbergen/oauthtest/server/model"
	"sync"
)

type oauthStateRepo struct {
	states map[string]*model.OAuthState
	mutex  sync.Mutex
}

func NewOAuthStateRepository() model.OAuthStateRepository {
	return &oauthStateRepo{
		states: map[string]*model.OAuthState{},
	}
}

func (r *oauthStateRepo) Put(key string, state *model.OAuthState) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	if _, err := r.get(key); err == nil {
		return model.ErrDuplicateKey
	}
	return r.put(key, state)
}

func (r *oauthStateRepo) put(key string, state *model.OAuthState) error {
	r.states[key] = state
	return nil
}

func (r *oauthStateRepo) Get(key string, delete ...bool) (*model.OAuthState, error) {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	return r.get(key)
}

func (r *oauthStateRepo) get(key string) (*model.OAuthState, error) {
	t := r.states[key]
	if t == nil {
		return nil, model.ErrTokenNotFound
	}
	return t, nil
}

func (r *oauthStateRepo) Delete(key string) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	return r.delete(key)
}

func (r *oauthStateRepo) delete(key string) error {
	if r.states[key] == nil {
		return model.ErrTokenNotFound
	}
	delete(r.states, key)
	return nil
}
