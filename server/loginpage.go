package server

import "net/http"

const loginPage = `<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">

</head>
<body>
	<div class="container">
	<h1>Login</h1>
    <form action="">
        <div><input type="text" name="uid" /></div>
        <div><input type="password" name="pwd" /></div>
		<div>
			<input type="submit" name="login" value="Login"/>
			<input type="submit" name="cancel" value="Cancel"/>
		 </div>
	</form>
	<div>    
		<div><a href="/admin">Protected page</a></div>
		<div><a href="/logout">Logout</a>
		<div><a href="/status">Status</a>
	</div>
	</div>
</body>
</html>`

func (s *Server) handleLoginPage() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		s.log.Printf("handleLoginPage")
		values := r.URL.Query()
		for k, vs := range values {
			for _, v := range vs {
				s.log.Printf("%s: %s\n", k, v)
			}
		}
		w.Header().Add("Content-Type", "text/html")
		w.Write([]byte(loginPage))
	}
}
