package server

import "net/http"

func (s *Server) handleCSS(css string) http.HandlerFunc {
	return s.handleStatic(css, "text/css")
}

func (s *Server) handleStatic(static string, contentType string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s.log.Printf("handleStatic: serving %s for %s", contentType, r.URL.String())
		w.Header().Add("Content-Type", contentType)
		w.Write([]byte(static))
	}
}
