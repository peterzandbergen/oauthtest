package redis

import (
	"encoding/json"

	"gitlab.com/peterzandbergen/oauthtest/server/model"

	"github.com/gomodule/redigo/redis"
)

// OAuthState is used for storing the state of the oauth redirect.
type oauthState struct {
	ID          string `json:"id"`
	RedirectURL string `json:"redirect_url"`
}

const (
	oauthStateKey          = "oauthstate"
	oauthStateKeySeparator = oauthStateKey + keySeparator
)

type oauthStateRepo struct {
	conn redis.Conn
}

func oauthStateFrom(st *model.OAuthState) *oauthState {
	return &oauthState{
		ID:          st.ID,
		RedirectURL: st.RedirectURL,
	}
}

func oauthStateTo(st *oauthState) *model.OAuthState {
	return &model.OAuthState{
		ID:          st.ID,
		RedirectURL: st.RedirectURL,
	}
}

func NewOAuthStateRepository(conn redis.Conn) model.OAuthStateRepository {
	return &oauthStateRepo{
		conn: conn,
	}
}

func (r *oauthStateRepo) Put(key string, state *model.OAuthState) error {
	b, err := json.Marshal(oauthStateFrom(state))
	if err != nil {
		return err
	}
	key = oauthStateKeySeparator + hashKey(key)
	if _, err := redis.String(r.conn.Do("SET", key, b, "NX")); err != nil {
		return model.ErrDuplicateKey
	}
	return nil
}
func (r *oauthStateRepo) Get(key string, delete ...bool) (*model.OAuthState, error) {
	key = oauthStateKeySeparator + hashKey(key)
	b, err := redis.Bytes(r.conn.Do("GET", key))
	if err != nil {
		return nil, err
	}
	if b == nil {
		return nil, model.ErrTokenNotFound
	}
	var st oauthState
	err = json.Unmarshal(b, &st)
	if err != nil {
		return nil, err
	}
	return oauthStateTo(&st), nil
}
func (r *oauthStateRepo) Delete(key string) error {
	key = oauthStateKeySeparator + key
	repl, err := redis.Int(r.conn.Do("DEL", key))
	if err != nil {
		return err
	}
	if repl == 0 {
		return model.ErrTokenNotFound
	}
	return nil
}
