package main

import (
	"testing"

	"gitlab.com/peterzandbergen/oauthtest/cf"
)

/*

CFGO_CLIENT_ID=0oag46r4lpaL5EksD5d5
CFGO_CLIENT_SECRET=BbVOFo2bPO7OPZJyiCmgfA-HAwmG1iObxeAlDUkZ
CFGO_DISCOVERY_URI="https://dev-9673843.okta.com/oauth2/default"
CFGO_SERVER_NAME=localhost
PORT=8080

*/

var testEnviron = []string{
	`CFGO_CLIENT_ID=0oag46r4lpaL5EksD5d5`,
	`CFGO_CLIENT_SECRET=BbVOFo2bPO7OPZJyiCmgfA-HAwmG1iObxeAlDUkZ`,
	`CFGO_DISCOVERY_URI=https://dev-9673843.okta.com/oauth2/default`,
	`CFGO_SERVER_NAME=localhost`,
	`PORT=8080`,
	`CFGO_OAUTH_SCOPES=openid profile email address phone offline_access`,
	`CFGO_CALLBACK_SCHEME=http`,
}

var testWSo2Environ = []string{
	`CFGO_CLIENT_ID=0oag46r4lpaL5EksD5d5`,
	`CFGO_CLIENT_SECRET=BbVOFo2bPO7OPZJyiCmgfA-HAwmG1iObxeAlDUkZ`,
	`CFGO_DISCOVERY_URI=https://localhost:9443/oauth2/token`,
	`CFGO_SERVER_NAME=localhost`,
	`PORT=8080`,
	`CFGO_OAUTH_SCOPES=openid profile email address phone offline_access`,
	`CFGO_CALLBACK_SCHEME=http`,
}

func TestConfig(t *testing.T) {
	var cfg *config
	var err error
	switch {
	case cf.OnEnvironment():
		cfg, err = configFromCfEnv()
	default:
		cfg = configFromEnv(testEnviron)
	}
	if cfg == nil {
		t.Fatalf("error getting cfg: %s", err)
	}
}

func TestRunConfig1(t *testing.T) {
	var cfg *config
	var err error
	cfg = configFromEnv(testWSo2Environ)
	if cfg == nil {
		t.Fatalf("error getting cfg: %s", err)
	}
	run(cfg)
}

func TestWSO2Config(t *testing.T) {
	var cfg *config
	var err error
	switch {
	case cf.OnEnvironment():
		cfg, err = configFromCfEnv()
	default:
		cfg = configFromEnv(testEnviron)
	}
	if cfg == nil {
		t.Fatalf("error getting cfg: %s", err)
	}
}
