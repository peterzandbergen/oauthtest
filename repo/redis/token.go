package redis

import (
	"encoding/json"
	"time"

	"gitlab.com/peterzandbergen/oauthtest/server/model"

	"github.com/gomodule/redigo/redis"
)

type tokenRepo struct {
	conn redis.Conn
	exp  int
}

const (
	tokenPrefix          = "token"
	keySeparator         = ":"
	tokenPrefixSeparator = tokenPrefix + keySeparator
)

type token struct {
	AccessToken  string    `json:"access_token"`
	TokenType    string    `json:"token_type,omitempty"`
	RefreshToken string    `json:"refresh_token,omitempty"`
	Expiry       time.Time `json:"expiry,omitempty"`
	IDToken      string    `json:"id_token"`
}

func tokenFrom(tk *model.Token) *token {
	return &token{
		AccessToken:  tk.AccessToken,
		TokenType:    tk.TokenType,
		RefreshToken: tk.RefreshToken,
		Expiry:       tk.Expiry,
		IDToken:      tk.IDToken,
	}
}

func tokenTo(tk *token) *model.Token {
	return &model.Token{
		AccessToken:  tk.AccessToken,
		TokenType:    tk.TokenType,
		RefreshToken: tk.RefreshToken,
		Expiry:       tk.Expiry,
		IDToken:      tk.IDToken,
	}
}

func NewTokenRepository(conn redis.Conn) model.TokenRepository {
	return &tokenRepo{
		conn: conn,
	}
}

// Put adds a key to the repo. Fails if the key already exists.
func (r *tokenRepo) Put(key string, token *model.Token) error {
	b, err := json.Marshal(tokenFrom(token))
	if err != nil {
		return err
	}
	key = tokenPrefixSeparator + hashKey(key)
	// Put the token if it does not exists.
	if _, err = redis.String(r.conn.Do("SET", key, b, "NX")); err != nil {
		return model.ErrDuplicateKey
	}
	return nil
}

func (r *tokenRepo) Get(key string) (*model.Token, error) {
	key = tokenPrefixSeparator + hashKey(key)
	b, err := redis.Bytes(r.conn.Do("GET", key))
	if err != nil {
		return nil, err
	}
	if b == nil {
		return nil, model.ErrTokenNotFound
	}
	var tk token
	err = json.Unmarshal(b, &tk)
	if err != nil {
		return nil, err
	}
	return tokenTo(&tk), nil
}

func (r *tokenRepo) Delete(key string) error {
	key = tokenPrefixSeparator + key
	repl, err := redis.Int(r.conn.Do("DEL", key))
	if err != nil {
		return err
	}
	if repl == 0 {
		return model.ErrTokenNotFound
	}
	return nil
}
