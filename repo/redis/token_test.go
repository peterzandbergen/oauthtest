package redis

import (
	"testing"

	"gitlab.com/peterzandbergen/oauthtest/server/model"

	"github.com/gomodule/redigo/redis"
)

func TestAddExisting(t *testing.T) {
	conn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		t.Fatalf("dial error: %s", err)
	}
	defer conn.Close()

	repo := NewTokenRepository(conn)

	tok := &model.Token{
		AccessToken: "{}",
	}
	err = repo.Put("1235", tok)
	if err != nil {
		t.Fatalf("put failed: %s", err)
	}
	err = repo.Put("1235", tok)
	if err != model.ErrDuplicateKey {
		t.Errorf("expected error: %s, got nil", model.ErrDuplicateKey)
	}
	err = repo.Delete("1235")
	if err != nil {
		t.Errorf("error deleting 1235: %s", err)
	}
}

func TestGetDeleteExisting(t *testing.T) {
	conn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		t.Fatalf("dial error: %s", err)
	}
	defer conn.Close()

	repo := NewTokenRepository(conn)

	const key = "test-delete-key"

	tok := &model.Token{
		AccessToken: "{}",
	}
	// Put a token
	err = repo.Put(key, tok)
	if err != nil {
		t.Errorf("put failed: %s", err)
	}
	err = repo.Delete(key)
	if err != nil {
		t.Errorf("error deleting token with key %s", key)
	}
}

func TestDeleteNonExisting(t *testing.T) {
	conn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		t.Fatalf("dial error: %s", err)
	}
	defer conn.Close()
	_ = conn

	repo := NewTokenRepository(conn)

	const key = "test-delete-non-existing-key"

	// Ensure key does not exist
	_ = repo.Delete(key)

	// Test delete
	err = repo.Delete(key)
	if err != model.ErrTokenNotFound {
		t.Errorf("expected error %s", model.ErrTokenNotFound)
	}
}
