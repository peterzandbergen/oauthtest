module gitlab.com/peterzandbergen/oauthtest

go 1.21

require (
	github.com/gomodule/redigo v1.8.9
	github.com/peterzandbergen/go-oidc v2.2.1+incompatible
	golang.org/x/oauth2 v0.15.0
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/pquerna/cachecontrol v0.2.0 // indirect
	golang.org/x/crypto v0.16.0 // indirect
	golang.org/x/net v0.19.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
	gopkg.in/square/go-jose.v2 v2.6.0 // indirect
)
