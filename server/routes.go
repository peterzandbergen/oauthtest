package server

import "net/http"

func (s *Server) routes() {
	s.router.HandleFunc("/",
		s.mwMethods(
			s.mwBlockPath(
				s.handleLandingPage(),
				"/favicon.ico",
				http.StatusNotFound),
			"GET"))

	s.router.HandleFunc("/authorization-code/callback",
		s.mwMethods(
			s.mwDisableCache(
				s.handleCodeCallback()),
			"GET"))

	s.router.HandleFunc("/admin",
		s.mwMethods(
			s.mwDisableCache(
				s.mwExtractBearerToken(
					s.mwProtectPage(
						s.handleAdminPage()))),
			"GET"))

	s.router.HandleFunc("/admin/admin",
		s.mwMethods(
			s.mwDisableCache(
				s.mwExtractBearerToken(
					s.mwProtectPage(
						s.handleAdminPage()))),
			"GET"))

	s.router.HandleFunc("/login",
		s.mwMethods(
			s.mwDisableCache(
				s.handleLoginPage()),
			"GET"))

	s.router.HandleFunc("/logout",
		s.mwMethods(
			s.mwDisableCache(
				s.mwExtractBearerToken(
					s.handleLogout())),
			"GET"))

	s.router.HandleFunc("/status",
		s.mwMethods(
			s.mwDisableCache(
				s.mwExtractBearerToken(
					s.handleStatusPage())),
			"GET"))
			
	s.router.HandleFunc("/css/bootstrap.min.css",
		s.mwMethods(
			s.mwSetCacheControl(
				s.handleCSS(commonCSS),
				"public", "max-age=31536000"),
			"GET"))
}
